package com.example.demo.controller;

import com.example.demo.server.api.EcoleApi;
import com.example.demo.server.model.Ecole;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HelloController implements EcoleApi {

    // // glpat-2RQ16rZ83RM-hz3TywEr

    @GetMapping("/hello")
    public String sayHello() {
        return "Hello its BALDE";
    }

    @Override
    public ResponseEntity<List<Ecole>> ecoleGet() {
        return null;
    }

    @Override
    public ResponseEntity<Ecole> ecoleIdGet(String id) {
        return null;
    }

    @Override
    public ResponseEntity<Void> ecoleIdPut(String id, Ecole body) {
        return null;
    }

    @Override
    public ResponseEntity<Void> ecolePost(Ecole body) {
        return null;
    }
}
