package com.example.demo.configurations;

import com.example.demo.client.ApiClient;
import com.example.demo.server.api.EcoleApi;
import com.example.demo.server.model.Ecole;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

import java.util.List;

@Configuration
public class APIConfiguration {

    @Bean
    public ApiClient apiClient() {
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath("localhost:8081");
        return apiClient;
    }
}
